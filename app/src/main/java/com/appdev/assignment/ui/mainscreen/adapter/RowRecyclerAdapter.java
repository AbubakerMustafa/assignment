package com.appdev.assignment.ui.mainscreen.adapter;

import java.util.List;

import com.appdev.assignment.R;
import com.appdev.assignment.model.Event;
import com.example.library.data.model.Result;
import com.example.library.utils.Constants;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;


public class RowRecyclerAdapter extends RecyclerView.Adapter<RowRecyclerAdapter.ViewHolder> {


    private List<Result> mItems;
    Context mContext;
    public RowRecyclerAdapter(Context context,List<Result> objects) {
        mContext = context;
        mItems = objects;

    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        public  ImageView mImageView;
        public  TextView mTextView;
        public View rootView;
        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            mImageView =(ImageView)itemView.findViewById(R.id.image);
            mTextView =(TextView)itemView.findViewById(R.id.title);
        }
    }

    @Override
    public int getItemCount() {

        return mItems.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Result item = mItems.get(position);

        if (item.getPosterPath() != null){

            Picasso.with(mContext).load(Constants.POSTER_BASE_URL+item.getPosterPath()).placeholder(mContext.getResources()
                    .getDrawable(R.drawable.logo)).into(holder.mImageView);

        }else {
            Picasso.with(mContext).load(Constants.POSTER_BASE_URL+item.getProfilePath()).placeholder(mContext.getResources()
                    .getDrawable(R.drawable.logo)).into(holder.mImageView);
        }

        holder.mTextView.setText(item.getTitle());

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new Event(item));
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int arg1) {
        LayoutInflater inflater =
                (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.item, parent, false);
        return new ViewHolder(convertView);
    }

}
