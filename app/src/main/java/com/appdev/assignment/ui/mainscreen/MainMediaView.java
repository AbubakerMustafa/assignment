package com.appdev.assignment.ui.mainscreen;

import com.appdev.assignment.base.MvpView;
import com.example.library.data.model.Error;
import com.example.library.data.model.Result;

import java.util.ArrayList;

public interface MainMediaView extends MvpView {

    void showItems(ArrayList<Result> mResults);
    void onError(Error error);
    void showEmptyTextError(int emptyText);
    String getQueryText();
    void showProgress();
    void hideProgress();
}
