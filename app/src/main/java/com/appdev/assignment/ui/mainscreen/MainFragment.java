package com.appdev.assignment.ui.mainscreen;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appdev.assignment.R;
import com.appdev.assignment.base.BaseFragment;
import com.appdev.assignment.model.Event;
import com.appdev.assignment.ui.detailscreen.DetailFragment;
import com.appdev.assignment.ui.mainscreen.adapter.MainRecyclerAdapter;
import com.example.library.data.model.Error;
import com.example.library.data.model.Result;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends BaseFragment implements MainMediaView {

    @BindView(R.id.recyclerview_root)
    RecyclerView mRecyclerView;
    Unbinder unbinder;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.img_clear)
    ImageView imgClear;
    @BindView(R.id.txt_search)
    TextView txtSearch;
    @BindView(R.id.search_bar_layout)
    RelativeLayout searchBarLayout;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private MainPresenter mainPresenter;
    private LinearLayoutManager mLayoutManager;
    private MainRecyclerAdapter mainRecyclerAdapter;
    private MainActivity activity;

    @Override
    public void initViews(View parentView, Bundle saveInstanceState) {

        activity = (MainActivity) getActivity();

        if (mainPresenter == null) {
            mainPresenter = new MainPresenter();
        }
        mainPresenter.attachView(this);

        setupRecyclerView();
    }

    @Override
    public int getLayoutId() {
        return R.layout.main;
    }

    @Override
    public void updateFragmentReference() {

    }

    @Override
    public void updateActionState(boolean action, View v) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick({R.id.txt_search, R.id.img_clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_search:
                String query = etSearch.getText().toString().trim();
                if (mainRecyclerAdapter != null && mainRecyclerAdapter.getDataSet() != null) {
                    mainRecyclerAdapter.clearAdapter();

                }
                mainPresenter.getMedia(query);
                hideKeyboard();


                break;

            case R.id.img_clear:

                etSearch.getText().clear();

                break;


        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showItems(ArrayList<Result> mResults) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            setAdapter(mResults);
        }

    }

    @Override
    public void onError(Error error) {
        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyTextError(int res) {
        Toast.makeText(getActivity(), getString(res), Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getQueryText() {
        return etSearch.getText().toString().trim();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setAdapter(ArrayList<Result> mResults) {

        //Sorting list
        Collections.sort(mResults, new Comparator<Result>() {
            @Override
            public int compare(final Result object1, final Result object2) {
                return object1.getMediaType().compareTo(object2.getMediaType());
            }
        });

        Map<String, List<Result>> mediaMap = mResults.stream().collect(Collectors.groupingBy(Result::getMediaType));
        List<String> keyList = new ArrayList<>(mediaMap.keySet());
        //Sorting key list alphabetically
        Collections.sort(keyList);
        mainRecyclerAdapter = new MainRecyclerAdapter(mediaMap, getActivity(),keyList);
        mRecyclerView.setAdapter(mainRecyclerAdapter);

    }

    private void setupRecyclerView() {

        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Event event) {

        DetailFragment fragment = new DetailFragment();
        fragment.setResult(event.getResult());
        activity.addFragment(fragment, "Detail Screen");

    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }
}
