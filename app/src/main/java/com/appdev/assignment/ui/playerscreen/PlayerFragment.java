package com.appdev.assignment.ui.playerscreen;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;


import com.appdev.assignment.R;
import com.appdev.assignment.base.BaseFragment;
import com.appdev.assignment.ui.playerscreen.component.VideoPlaybackManager;
import com.appdev.assignment.ui.mainscreen.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PlayerFragment extends BaseFragment {

    private final String TAG = "TimeLapsePlayerFragment";

    private final String STATE_IS_FULLSCREEN = "fullscreenstate";
    private final String STATE_CURRENT_PLACE = "currentplaceinvideo";

    Unbinder unbinder;
    @BindView(R.id.videoSurface)
    SurfaceView videoSurface;
    @BindView(R.id.load_video_progress)
    ProgressBar loadVideoProgress;
    @BindView(R.id.start_video)
    ImageButton startVideo;
    @BindView(R.id.video_start_playing_container)
    RelativeLayout videoStartPlayingContainer;
    @BindView(R.id.videoSurfaceContainer)
    FrameLayout videoSurfaceContainer;

    private boolean isInstanceStateNull;

    private boolean isShowFullScreen = true;
    private int seekToTime = -1;
    private VideoPlaybackManager videoPlaybackManager;
    private MainActivity activity;

    @Override
    public void initViews(View parentView, Bundle saveInstanceState) {

        unbinder = ButterKnife.bind(this, parent);
        activity = (MainActivity) getActivity();

        isInstanceStateNull = (saveInstanceState == null);
        assert activity != null;
        activity.setLandScape(true);
        activity.setFullScreen(true);
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


       // initPlayer();

        initVideoPlayer();
    }

    @Override
    public int getLayoutId() {
        return R.layout.player_fragment_layout;
    }

    @Override
    public void updateFragmentReference() {

    }

    @Override
    public void updateActionState(boolean action, View v) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putBoolean(STATE_IS_FULLSCREEN,isShowFullScreen);
        outState.putInt(STATE_CURRENT_PLACE,seekToTime);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }


    private void initVideoPlayer() {

        videoPlaybackManager = new VideoPlaybackManager(getActivity(), (FrameLayout) videoSurfaceContainer,
                (SurfaceView) videoSurface, (ProgressBar) loadVideoProgress);

        videoPlaybackManager.setFullscreenBtnClickListener(new VideoPlaybackManager.FullscreenBtnClickListener() {
            @Override
            public void onFullScreenClicked() {

                isShowFullScreen = !isShowFullScreen;
                seekToTime = videoPlaybackManager != null ? videoPlaybackManager.getCurrentPosition() : -1;

                int activityOrientation = (isShowFullScreen) ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

                activity.setRequestedOrientation(activityOrientation);
            }
        });


        videoPlaybackManager.setPlayerListener(new VideoPlaybackManager.PlayerListener() {

            @Override
            public void onPlayerReady() {


                if (seekToTime != -1)
                    videoPlaybackManager.startPlayer(seekToTime);
                else
                    videoPlaybackManager.startPlayer();

                videoPlaybackManager.pause();

                if (isInstanceStateNull)

                    if (videoStartPlayingContainer != null) {
                        videoStartPlayingContainer.setVisibility(View.VISIBLE);
                    }


                if (videoSurface != null) {
                    videoSurface.setBackgroundResource(0);
                }


            }

            @Override
            public void onVideoCompleted() {
                //todo inspect why getActivity() returns null at times.
                if (getActivity() != null) {
                    Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.abc_fade_in);

                }
            }


            @Override
            public void onPlayPressed() {
                if (videoStartPlayingContainer != null) {
                    videoStartPlayingContainer.setVisibility(View.GONE);
                }

            }

            @Override
            public void onPausePressed() {

                if (videoStartPlayingContainer != null) {
                    videoStartPlayingContainer.setVisibility(View.VISIBLE);
                }


            }
        });

        startVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (videoStartPlayingContainer != null) {
                    videoStartPlayingContainer.setVisibility(View.GONE);
                }
                if (videoPlaybackManager != null) {
                    videoPlaybackManager.startPlayer();
                }

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
