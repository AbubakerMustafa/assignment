package com.appdev.assignment.ui.mainscreen;


import com.appdev.assignment.R;
import com.appdev.assignment.base.BasePresenter;
import com.appdev.assignment.util.GeneralUtils;
import com.example.library.MyApplication;
import com.example.library.data.enums.ErrorType;
import com.example.library.data.model.ServerResponse;
import com.example.library.data.model.Error;
import com.example.library.data.remote.MediaService;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;


public class MainPresenter extends BasePresenter<MainMediaView> {

    private Subscription mSubscription;
    private Error defaultError;


    public MainPresenter() {
        defaultError = new Error();
    }


    @Override
    public void attachView(MainMediaView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void getMedia(String query) {
        checkViewAttached();

        if (query.isEmpty()){
            getMvpView().hideProgress();
            getMvpView().showEmptyTextError(R.string.alert);
            return;

        }

        if (!GeneralUtils.isNetworkConnected(MyApplication.get().getApplicationContext())) {

            defaultError.setType(ErrorType.NETWORK);
            defaultError.setMessage(MyApplication.get().getApplicationContext().getString(R.string.network_error));
            getMvpView().onError(defaultError);
            getMvpView().hideProgress();
            return;
        }

        if (mSubscription != null) mSubscription.unsubscribe();
        getMvpView().showProgress();
        MediaService mediaService = MyApplication.get().getMediaService();
        mSubscription = mediaService.getMediaList(query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(MyApplication.get().defaultSubscribeScheduler())
                .subscribe(new Subscriber<ServerResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (getMvpView() == null || defaultError == null) {
                            return;
                        }

                        defaultError.setMessage(e.getMessage());
                        getMvpView().onError(defaultError);
                        getMvpView().hideProgress();
                    }

                    @Override
                    public void onNext(ServerResponse serverResponse) {
                        getMvpView().hideProgress();
                        parseResponse(serverResponse);

                    }

                });
    }


    private void parseResponse(ServerResponse resp) {
        if (resp == null) {
            getMvpView().onError(defaultError);
            return;
        }

        getMvpView().showItems(resp.getResults());
    }


}
