package com.appdev.assignment.ui.mainscreen;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.appdev.assignment.R;
import com.appdev.assignment.base.BaseActivity;
import com.appdev.assignment.ui.playerscreen.PlayerFragment;
import com.appdev.assignment.util.SystemBarTintManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    private boolean isLandScape = false;

    private boolean isMaterialStatusBar;
    private SystemBarTintManager systemBarTintManager;

    @Override
    public void initViews(Bundle savedInstanceState) {
        ButterKnife.bind(this);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //toolbarTitle.setText("Main");
        startMainScreen();


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {


                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);

                if (fragment != null) {

                    toolbarTitle.setText(fragment.getTag());

                    if (fragment instanceof PlayerFragment) {
                        myToolbar.setVisibility(View.GONE);
                    }else {
                        myToolbar.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    private void startMainScreen() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, new MainFragment(),"Search Screen");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void replaceFragment(Fragment fragment, String tag) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R
                .anim.slide_out_down);
        fragmentTransaction.replace(R.id.container, fragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void addFragment(Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_from_right, R.anim.slide_in_from_left, R
                .anim.slide_out_from_left);
        fragmentTransaction.add(R.id.container, fragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setFullScreen(boolean fullScreen) {

        int flagToAdd = fullScreen ? WindowManager.LayoutParams.FLAG_FULLSCREEN : WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN;
        int flagToClear = fullScreen ? WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN : WindowManager.LayoutParams.FLAG_FULLSCREEN;

        getWindow().addFlags(flagToAdd);
        getWindow().clearFlags(flagToClear);

        if (fullScreen) {
            myToolbar.setVisibility(View.GONE);
        } else {
            myToolbar.setVisibility(View.VISIBLE);
        }

        if (isMaterialStatusBar)
            systemBarTintManager.setStatusBarTintEnabled(!fullScreen);

    }

    private void clearBackStack() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        if (isLandScape) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setFullScreen(false);
            isLandScape = false;
            super.onBackPressed();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setLandScape(boolean landScape) {
        isLandScape = landScape;
    }
}
