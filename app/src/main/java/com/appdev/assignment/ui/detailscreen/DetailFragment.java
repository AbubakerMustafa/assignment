package com.appdev.assignment.ui.detailscreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appdev.assignment.R;
import com.appdev.assignment.base.BaseFragment;
import com.appdev.assignment.ui.mainscreen.MainActivity;
import com.appdev.assignment.ui.playerscreen.PlayerFragment;
import com.example.library.data.model.Result;
import com.example.library.utils.Constants;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DetailFragment extends BaseFragment {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.play_button)
    Button playButton;
    Unbinder unbinder;
    private Result result;
    private MainActivity activity;



    @Override
    public void initViews(View parentView, Bundle saveInstanceState) {

       unbinder = ButterKnife.bind(this, parentView);
       activity = (MainActivity) getActivity();

        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (result != null){

            title.setText(result.getTitle());
            description.setText(result.getOverview());

            if (result.getMediaType().equals("tv") || result.getMediaType().equals("movie")){
                playButton.setVisibility(View.VISIBLE);
            }else {
                playButton.setVisibility(View.GONE);
            }


            if (result.getPosterPath() != null){

                Picasso.with(getActivity()).load(Constants.POSTER_BASE_URL+result.getPosterPath()).placeholder(getActivity().getResources()
                        .getDrawable(R.drawable.logo)).into(imageView);

            }else {
                Picasso.with(getActivity()).load(Constants.POSTER_BASE_URL+result.getProfilePath()).placeholder(getActivity().getResources()
                        .getDrawable(R.drawable.logo)).into(imageView);
            }

        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_detail;
    }

    @Override
    public void updateFragmentReference() {

    }

    @Override
    public void updateActionState(boolean action, View v) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.play_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.play_button:

                activity.addFragment(new PlayerFragment(),"");
                break;


        }
    }

    public void setResult(Result result){
        this.result = result;
    }

    @Override
    public void onPause() {
        super.onPause();
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
}
