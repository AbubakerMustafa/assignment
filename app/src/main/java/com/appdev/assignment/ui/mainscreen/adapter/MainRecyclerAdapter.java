package com.appdev.assignment.ui.mainscreen.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appdev.assignment.R;
import com.example.library.data.model.Result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder> {

    private Map<String,List<Result>> mediaMap;
    private Context mContext;
    private RowRecyclerAdapter rowsRecyclerAdapter;
    private List<String> keyList;


    public MainRecyclerAdapter(Map<String,List<Result>> objects,Context context,List<String> keyList) {
        mContext = context;
        mediaMap = objects;
        this.keyList = keyList;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public  RecyclerView mRecyclerViewRow;
        public TextView type;
        public ViewHolder(View itemView) {
            super(itemView);
            mRecyclerViewRow =(RecyclerView)itemView.findViewById(R.id.recyclerView_row);
            type =(TextView)itemView.findViewById(R.id.txt_media_type);

        }
    }

    @Override
    public int getItemCount() {

        return mediaMap.keySet().size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            Collections.sort(keyList);
            String key = keyList.get(position);
            List<Result> value =   mediaMap.get(key);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
            holder.mRecyclerViewRow.setLayoutManager(layoutManager);
            holder.mRecyclerViewRow.setHasFixedSize(true);
            holder.type.setText(key);
            rowsRecyclerAdapter = new RowRecyclerAdapter(mContext,value);
            holder.mRecyclerViewRow.setAdapter(rowsRecyclerAdapter);

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);

        return new ViewHolder(itemView);
    }

    public Map<String,List<Result>> getDataSet() {
        return mediaMap;
    }

    public void clearAdapter() {
        mediaMap.clear();
        notifyDataSetChanged();
    }

}
