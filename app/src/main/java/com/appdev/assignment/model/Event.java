package com.appdev.assignment.model;

import com.example.library.data.model.Result;

public class Event {

    private Result result;

    public Event(Result result){
        this.result = result;
    }

    public Result getResult(){
        return result;
    }
}
