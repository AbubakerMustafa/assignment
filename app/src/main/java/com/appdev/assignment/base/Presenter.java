package com.appdev.assignment.base;

public interface Presenter<V> {

    void attachView(V view);

    void detachView();

}