package com.appdev.assignment.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;


import butterknife.ButterKnife;


public abstract class BaseFragment extends Fragment {
    public static final String ARGS_INSTANCE = "com.appdev.moduletest";

    public View parent;
    int mInt = 0;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mInt = args.getInt(ARGS_INSTANCE);
        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parent = inflater.inflate(getLayoutId(), container, false);
        parent.setClickable(true);
        parent.requestFocus();
        ButterKnife.bind(this, parent);
        initViews(parent,savedInstanceState);
        updateFragmentReference();
        return parent;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }


    public abstract void initViews(View parentView , Bundle saveInstanceState);

    public abstract int getLayoutId();

    public abstract void updateFragmentReference();

    public abstract void updateActionState(boolean action, View v);


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
