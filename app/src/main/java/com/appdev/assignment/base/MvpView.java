package com.appdev.assignment.base;

import android.content.Context;

public interface MvpView {

    Context getContext();
}
