package com.appdev.assignment.util;

import com.example.library.data.model.Result;
import com.example.library.data.model.ServerResponse;

import java.util.ArrayList;

/**
 * Created by abubaker on 24/05/2018.
 */

public class MockServerResponse {

    public static ServerResponse responseFromServer() {

        ServerResponse response = new ServerResponse();
        response.setResults(getDummyResults());
        return response;
    }


    private static ArrayList<Result> getDummyResults() {

        ArrayList<Result> mResults = new ArrayList<Result>();
        // Creating first Mock Object
        Result person = new Result();
        person.setTitle("SteveJobs");
        person.setAdult(true);
        person.setId(101);
        person.setMediaType("person");
        person.setPosterPath("");

        Result movie = new Result();
        movie.setTitle("Troy");
        movie.setAdult(true);
        movie.setId(102);
        movie.setMediaType("movie");
        movie.setPosterPath("");
        mResults.add(movie);

        Result tv = new Result();
        tv.setTitle("Friends");
        tv.setAdult(true);
        tv.setId(103);
        tv.setMediaType("tv");
        tv.setPosterPath("");
        mResults.add(tv);

        return mResults;
    }

}
