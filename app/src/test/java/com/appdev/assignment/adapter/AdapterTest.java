package com.appdev.assignment.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.appdev.assignment.BuildConfig;
import com.appdev.assignment.ui.mainscreen.adapter.MainRecyclerAdapter;
import com.appdev.assignment.util.MockServerResponse;
import com.example.library.data.model.Result;
import com.example.library.data.model.ServerResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by abubaker on 28/05/2018.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class AdapterTest {

    private Context context;

    @Before
    public void setup() {
        context = RuntimeEnvironment.application;
    }


    @Test
    public void adapterViewRecyclingCaption() {


        // Set up input
        ServerResponse response = MockServerResponse.responseFromServer();

        Map<String, List<Result>> mediaMap = response.getResults().stream().collect(Collectors.groupingBy(Result::getMediaType));
        List<String> keys = new ArrayList<>(mediaMap.keySet());
        Collections.sort(keys);
        MainRecyclerAdapter mainRecyclerAdapter = new MainRecyclerAdapter(mediaMap,context,keys);
        RecyclerView rvParent = new RecyclerView(context);
        rvParent.setLayoutManager(new LinearLayoutManager(context));

        // Run test
        MainRecyclerAdapter.ViewHolder viewHolder = mainRecyclerAdapter.onCreateViewHolder(rvParent, 0);

        mainRecyclerAdapter.onBindViewHolder(viewHolder, 0);
        // JUnit Assertion

        assertEquals("movie", viewHolder.type.getText().toString());

        assertThat(2, is(equalTo(mainRecyclerAdapter.getItemCount())));



    }
}
