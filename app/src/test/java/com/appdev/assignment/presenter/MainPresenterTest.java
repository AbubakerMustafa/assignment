package com.appdev.assignment.presenter;

import com.appdev.assignment.App;
import com.appdev.assignment.BuildConfig;
import com.appdev.assignment.R;
import com.appdev.assignment.ui.mainscreen.MainMediaView;
import com.appdev.assignment.ui.mainscreen.MainPresenter;
import com.appdev.assignment.util.MockServerResponse;
import com.example.library.data.model.ServerResponse;
import com.example.library.data.remote.MediaService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import rx.Observable;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 27, manifest=Config.NONE, application = App.class)
public class MainPresenterTest {

    private MainMediaView mView;
    private MediaService mediaService;
    private MainPresenter mainPresenter;


    @Before
    public void initMocksAndSetup() throws Exception {
        MockitoAnnotations.initMocks(this);
        App application = (App) RuntimeEnvironment.application;
        application.setDefaultSubscribeScheduler(Schedulers.immediate());

        mediaService = mock(MediaService.class);
        // Mock the retrofit service so we don't call the API directly
        application.setMediaService(mediaService);
        mView = mock(MainMediaView.class);
        mainPresenter = new MainPresenter();
        mainPresenter.attachView(mView);

    }

    @After
    public void tearDown() {
        mainPresenter.detachView();
    }


    @Test
    public void shouldShowErrorMessageWhenQueryIsEmpty() {
        when(mView.getQueryText()).thenReturn("");
        mainPresenter.getMedia(mView.getQueryText());
        verify(mView).showEmptyTextError(R.string.alert);
    }

    @Test
    public void shouldShowQueryResults() {
        String query = "jobs";
        ServerResponse response = MockServerResponse.responseFromServer();
        when(mediaService.getMediaList(query)).thenReturn(Observable.just(response));
        mainPresenter.getMedia(query);
        verify(mView).showProgress();
        verify(mView).hideProgress();
        verify(mView).showItems(response.getResults());

    }

    @Test
    public void shouldShowDefaultErrorMessageWhenSomeErrorOccurred() {
        String query = "jobs";
        when(mediaService.getMediaList(query)).thenReturn(Observable.<ServerResponse>error(new RuntimeException("error")));
        mainPresenter.getMedia(query);
        verify(mView).hideProgress();
    }
}