package com.example.library;

import android.app.Application;

import com.example.library.data.remote.MediaService;

import rx.Scheduler;
import rx.schedulers.Schedulers;

public class MyApplication extends Application {

    private static MyApplication mInstance = null;
    private Scheduler defaultSubscribeScheduler;
    private MediaService mediaService;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static MyApplication get() {
        if (mInstance == null){
            mInstance = new MyApplication();
        }
        return mInstance;
    }

    public MediaService getMediaService() {
        if (mediaService == null) {
            mediaService = MediaService.Factory.create();
        }
        return mediaService;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }


    //For setting mocks during testing
    public void setMediaService(MediaService routeService) {
        this.mediaService = routeService;
    }

    //User to change scheduler from tests
    public void setDefaultSubscribeScheduler(Scheduler scheduler) {
        this.defaultSubscribeScheduler = scheduler;
    }
}
