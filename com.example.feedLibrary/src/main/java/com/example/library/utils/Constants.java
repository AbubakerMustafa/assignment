package com.example.library.utils;

public class Constants {

    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String SEARCH_MULTI_END_POINT =
            "search/multi?api_key=3d0cda4466f269e793e9283f6ce0b75e&language=en-US&include_adult=false&page=1";
    public static final String POSTER_BASE_URL = "http://image.tmdb.org/t/p/w185";
    public static final int TIME_OUT = 30;
    public static final String DEFAULT_TIME_FORMAT="yyyy-MM-dd'T'HH:mm:ssZ";
}
